﻿using AspCourse.Domain.Entities;
using AspCourse.Domain.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AspCourse.Services
{
    public class EmployeesService : IEmployeesService
    {
        private readonly IUnitOfWorkFactory _uowFactory;

        public EmployeesService(IUnitOfWorkFactory unitOfWorkFactory)
        {
            _uowFactory = unitOfWorkFactory;
        }

        public async Task<Employee> CreateNewAsync(Employee newEmployee)
        {
            using (var uow = _uowFactory.Create())
            {
                var created = await uow.EmployeesRepository.CreateNewAsync(newEmployee);
                await uow.SaveChangesAsync();
                return created;
            }
        }

        public async Task<IEnumerable<Employee>> GetAllAsync()
        {
            using (var uow = _uowFactory.Create())
            {
                var res = await uow.EmployeesRepository.GetAllAsync();
                return res;
            }
        }
    }
}
