﻿using AspCourse.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AspCourse.Domain.Interfaces
{
    public interface IEmployeesRepository
    {
        Task<IEnumerable<Employee>> GetAllAsync();
        Task<Employee> CreateNewAsync(Employee newEmployee);
    }
}
