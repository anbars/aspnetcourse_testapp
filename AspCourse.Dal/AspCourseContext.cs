﻿using AspCourse.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AspCourse.Dal
{
    public class AspCourseContext : DbContext
    {
        public AspCourseContext(string nameOrConnectionString) : base(nameOrConnectionString)
        {
            Database.SetInitializer(new DropCreateDatabaseIfModelChanges<AspCourseContext>());
        }

        public DbSet<Employee> Employees { get; set; }
    }
}
