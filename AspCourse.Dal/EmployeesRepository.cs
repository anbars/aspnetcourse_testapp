﻿using AspCourse.Domain.Entities;
using AspCourse.Domain.Interfaces;
using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;

namespace AspCourse.Dal
{
    public class EmployeesRepository : RepositoryBase<Employee>, IEmployeesRepository
    {
        public EmployeesRepository(DbContext context) : base(context)
        {
        }

        public Task<Employee> CreateNewAsync(Employee newEmployee)
        {
            var added = Set.Add(newEmployee);
            return Task.FromResult(added);
        }

        public async Task<IEnumerable<Employee>> GetAllAsync()
        {
            var res = await Set.ToListAsync();
            return res;
        }
    }
}
