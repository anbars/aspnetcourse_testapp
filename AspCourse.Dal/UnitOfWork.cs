﻿using AspCourse.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AspCourse.Dal
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly AspCourseContext _context;

        public UnitOfWork(string nameOrConnectionString)
        {
            _context = new AspCourseContext(nameOrConnectionString);
        }

        public IEmployeesRepository EmployeesRepository => new EmployeesRepository(_context);

        public void Dispose()
        {
            _context?.Dispose();
        }

        public Task SaveChangesAsync()
        {
            return _context.SaveChangesAsync();
        }
    }
}
