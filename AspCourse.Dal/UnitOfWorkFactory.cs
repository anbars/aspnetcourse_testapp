﻿using AspCourse.Domain.Interfaces;
using System.Configuration;

namespace AspCourse.Dal
{
    public class UnitOfWorkFactory : IUnitOfWorkFactory
    {
        private static readonly string ConnectionStringName;

        static UnitOfWorkFactory()
        {
            ConnectionStringName = ConfigurationManager.AppSettings["Dal.ConnectionStringName"] ?? "Default";
        }

        public IUnitOfWork Create() => new UnitOfWork(ConnectionStringName);
    }
}
