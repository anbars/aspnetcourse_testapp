﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AspCourse.Dal
{
    public abstract class RepositoryBase<TEntity> where TEntity : class
    {
        private readonly DbContext _context;

        protected RepositoryBase(DbContext context)
        {
            _context = context;
        }

        protected DbSet<TEntity> Set => _context.Set<TEntity>();
    }
}
