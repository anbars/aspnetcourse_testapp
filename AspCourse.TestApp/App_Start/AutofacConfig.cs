﻿using AspCourse.Dal;
using AspCourse.Domain.Interfaces;
using AspCourse.Services;
using Autofac;
using Autofac.Integration.Mvc;
using System.Web.Mvc;

namespace AspCourse.TestApp
{
    public static class AutofacConfig
    {
        public static void ConfigureContainer()
        {
            var builder = new ContainerBuilder();

            builder.RegisterControllers(typeof(MvcApplication).Assembly);

            builder.RegisterType<UnitOfWorkFactory>().As<IUnitOfWorkFactory>();
            builder.RegisterType<EmployeesService>().As<IEmployeesService>();

            var container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }
    }
}
