﻿using AspCourse.Domain.Entities;
using AspCourse.Domain.Interfaces;
using AspCourse.TestApp.Models;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace AspCourse.TestApp.Controllers
{
    public class EmployeesController : Controller
    {
        private readonly IEmployeesService _employeesService;

        public EmployeesController(IEmployeesService employeesService)
        {
            _employeesService = employeesService;
        }

        public async Task<ActionResult> Index()
        {
            var employees = await _employeesService.GetAllAsync();

            return View(employees.Select(e => new EmployeeListViewModel { Id = e.Id, Name = e.Name, Position = e.Position }));
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View(new EmployeeCreationViewModel());
        }

        [HttpPost]
        public async Task<ActionResult> Create(EmployeeCreationViewModel model)
        {
            if (ModelState.IsValid)
            {
                var res = await _employeesService.CreateNewAsync(new Employee { Name = model.Name, Position = model.Position });
                TempData["success"] = "New employee successfully added!";
                return RedirectToAction("Index", "Home");
            }

            return View(model);
        }
    }
}