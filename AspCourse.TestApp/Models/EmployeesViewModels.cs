﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AspCourse.TestApp.Models
{
    public class EmployeeListViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Position { get; set; }
    }
    public class EmployeeCreationViewModel
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public string Position { get; set; }
    }
}
